#!/bin/bash
# ------------------------------------------------------------------
# [Author] Luis De Marquez / course IT FDN 140 B
#          Script name: security_check.sh
#          Description: Assignment 4
# ------------------------------------------------------------------
#Variables
workdir=/usr/local/bin
logfile=security_log.txt
now=`date +"%m/%d/%y"`
# Execution of script
echo "*******************************" >> $workdir/$logfile
echo "User executing the Security report is:  $USER" >> $workdir/$logfile
echo "*** Security report for: $HOSTNAME " >> $workdir/$logfile
echo "*** Report Date: $now " >> $workdir/$logfile
echo "*** Running Report script security_check.sh "  >> $workdir/$logfile
echo "*** Find directories open  -o=rwx" >> $workdir/$logfile
find / -type d -perm -o=rwx |xargs ls -ld >> $workdir/$logfile
echo >> $workdir/$logfile
echo "*** Find TCP and UDP open ports" >> $workdir/$logfile
ss -nutlp >> $workdir/$logfile
echo >> $workdir/$logfile
echo "*** Report all members of group wheel" >> $workdir/$logfile
lid -g wheel >> $workdir/$logfile 
echo >> $workdir/$logfile
echo >> $workdir/$logfile
echo "**** End security report ******" >> $workdir/$logfile
echo "*******************************" >> $workdir/$logfile
